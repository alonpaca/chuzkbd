$(function() {
			$("[data-toggle='tooltip']").tooltip();
			$("[data-toggle='popover']").popover();
			$('.carousel').carousel({
				interval: 2000
			});

			$('#contacto').on('show.bs.modal',function (e) {
				console.log('el modal contacto se está mostrando.');
				//<button class="btn btn-primary btn-warning" data-toggle="modal" data-target="#contacto" disabled>Contacto</button>							
				$('#contactoBtn').removeClass('btn-info');
				$('#contactoBtn').addClass('btn-danger');				
				$('#contactoBtn').prop('disabled',true);

			});

			$('#contacto').on('shown.bs.modal',function (e) {
				console.log('el modal contacto se mostró.');
			});

			$('#contacto').on('hide.bs.modal',function (e) {
				console.log('el modal contacto se oculta..');
			});

			$('#contacto').on('hidden.bs.modal',function (e) {
				console.log('el modal contacto se ocultó.');
				$('#contactoBtn').addClass('btn-info');
				$('#contactoBtn').removeClass('btn-danger');
				$('#contactoBtn').prop('disabled',false);
			});

			

		});